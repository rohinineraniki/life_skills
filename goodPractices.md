# Good Practices for Software Development

## Question 1

### What is your one major takeaway from each one of the 6 sections. So 6 points in total.

1. Make sure to ask questions and seek clarity in the meeting itself.
2. If current deadline will not met due to any reason inform to team members.
3. Explain the problem clearly, mention the solutions you tried out to fix the problem.
4. Join the meetings 5-10 mins early to get some time with your team members.
5. Be available when someone replies to your message. Things move faster if it's a real-time conversation as compared to a back and forth every hour.
6. Work when you work, play when you play is a good thumb rule.

## Question 2

### Which area do you think you need to improve on? What are your ideas to make progress in that area?

Get to know your teammates

I want to improve on these point, My ideas to progress are starting conversation with teammates, discussing about improvement in projects or any doughts.
