# Energy Management

## 1. Manage Energy not Time

### Question 1:

#### What are the activities you do that make you relax - Calm quadrant?

Relax- calm quadrant have serenity, expectation, sleepiness, relaxation

1. First thing I will do when am in hurry is will say cool..cool to my mind. It will lead to calm down for a minute and take a look into work.
2. When I feel drowsy during working listening music for 10 minutes makes me relax then go back to my work.
3. Talking with friends makes relax.

### Question 2:

#### When do you find getting into the Stress quadrant?

Stress quadrent have Anger, Tension, Disgust, Fear

1. The things which not happens in expected way causes anger.
2. I feel tension when work not completed ontime and all others are done with it.
3. I feel fear when my higher officials get serious on me about some work dome mistakenly.

### Question 3:

#### How do you understand if you are in the Excitement quadrant?

Excitement quadrant have Surprise, Happiness, Satisfaction, Enjoyment

1. I share that with friends and family.
2. Take a moment and enjoy it.

## 4. Sleep is your superpower

### Question 4

#### Paraphrase the Sleep is your Superpower video in detail.

Matt Walker TED speak :

Lack of sleep causes the life span for dacade,from some observations, we need sleep after learning to save what we learn, and also we need sleep before learning to get all new information.

Taken two groups sleep and sleep deprivation group, sleep group takes 8 hours of sleep, sleep deprivation group are not have sleep at all, after that taken MRI scans of both groups, significant changes are observed that 40 percent of ability to remember is deficit in sleep deprivation.

In brain we have 2 hippocampus these are memory banks.By placing electrodes to head powerful vibes are happen during sleep, memory trasport happens in this deepest sleep. Worse sleep leads to worse memory.

Aging is happens because many reasons but we can't control remaining things one we control is sleep, so to get good sleep not use sleeping pills it causes unrealistic sleep.

#### About electrode technology:

Instead they are developing direct current brain simulator it is small amount of voltage send to brain it gives measurable impact. When we apply this voltage to youngers brain it amplifies the amount of deep sleep and remembering things also get improved. When we do same technology for older adults to restore back deep sleep and ability of remember.

Sleep is fuel of body, global experiment when we loose one hour of sleep it causes 24 percent of increase of heart attack. Gain of sleep causes deduct 21 Percent of heart attack.

Natural killer cells like secret service agent of over immune system. It remove dangerous unwanted elements. When 4 hours of sleep happens it reduces 70 percent of natural killer cells. It gives immune deficiency. Shorting of sleep means shorting life and increase in the develop cancer. He taken healthy group of people and make them 6 hrs of sleep for 1 week it deducts genes ability which causes lack of immunity, inflammation,tumor promoting,cardiovascular disease

#### Key points to maintain good sleep:

Regularity in sleep - It improve quality of sleep
Keep environment cool
Sleep in life not optional luxury, sleep is life support system it is a biological activity.

### Question 5

#### What are some ideas that you can implement to sleep better?

1. Cant compromise on sleep timings.
2. Get relax when going to sleep, not in pressure.
3. Less disturbence during sleep time.

## 5. Brain Changing Benefits of Exercise

### Question 6

#### Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

Wendy Suzuki TED talk:

She is neuroscientist, she do research work.
Exercise have immediate effects of brain.
Single workout you do will immediatly increase levels of neurotransmitters.

single workout improve your ability to shift and focus attention. It lasts for 2 hours.

Exercise changes brain anatomy physiology and function.

Exercise actually produces brand new brain cells in hippocampus, it improve your long-term memory and increase volume in hippocampus

Do exercises which increase heart rate 3-4 times in a week and 30 minutes every time.

Bringing exercise in your life gives happier and more protective brain and life.

### Question 7

#### What are some steps you can take to exercise more?

I will include 30 minutes of exercise in my daily life.
