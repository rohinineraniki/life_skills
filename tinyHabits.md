# Tiny Habits

### 1. Tiny Habits - BJ Fogg

## Question 1

### Your takeaways from the video (Minimum 5 points)

He teach in Stanford University.
They practiced tiny habit.They tried some celebrations like telling ourself to "You are awesome and bingo"
Practice changing of your behavior is not that much complicated as we think.

There are 15 ways of behavioural changes.

In long term change there is only two ways to get it done.
Change environment is reliable way to change your behavior
He tried many tiny habits on weight loss finally success
"When you know how to create tiny habits, You can change your life forever."

What causes behavior change?

- Motivation to do it
- ability to do it
- Trigger to do it

If you use an existing behaviour in your life and you put tiny behavior **after** it you can use existing behavior as trigger.

Ater I do something**\_**, I will do my tiny habit\_\_\_.

Plant a tiny seed in the right spot it will grow without coaxing

# 2. Tiny Habits by BJ Fogg - Core Message

## Question 2

### Your takeaways from the video in as much detail as possible

B=MAP is universal formula for behavior

B = Behavior
M = Motivation
A = Ability
P = Prompt

Tiny habits behavior:

1. Shrink the behavior : Easy to do in <30 seconds
2. Identify action prompt : External prompts- having alarms
   Internal prompts
   Action prompts - fit tiny habit in between daily actions
3. Grow your habit with some shine

Celecrate after doing tiny habit done
it gives confidence, motivation to do again, frequency of success increases.

## Question 3

### How can you use B = MAP to make making new habits easier?

B = MAP
To build tiny habits

Motivation : It help me to start the tiny habit and stick on to it
Ability : Tiny habits which I take is must have possible in way.
Prompt : Attach my tiny habit to daily work like when pickup my phone or washing hands etc,.

## Question 4

### Why it is important to "Shine" or Celebrate after each successful completion of habit?

It is important to Celebrate after each successful completion of habit why because

- It gives self happiness
- It gives motivation to do that tiny habit again
- We feel successful for many times it gives positiveness
- Builds confidence

# 3. 1% Better Every Day Video

## Question 5

### Your takeaways from the video (Minimum 5 points)

1% better every day

There are four stages of habit formation

Noticing :

Plan to implement what we are thinking to do, Making plan and execute it. So do plan like when where what the plan is.

Take a if else condition also, if we cant make the plan then what need to do.

Wanting :

Environment often influence that we want things simply because they are an option.

Put more steps between you and bad behaviors, fewer steps between you and good behavior.

Doing :

Quality vs.Quantity

Quality makes good outcome than quantity
Any habit can started less than 2 minutes
Optimize the starting line - Initiate the habit.

Liking :

The only reason we repeat behaviors is because we like them. You need to experience rewards along the way.

After some days follow "Dont break the chain" Get back on track

Your identity emerges out of the habits that you have.

# 4. Book Summary of Atomic Habits

## Question 6

### Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

There are 3 layers of behavior change

Outcomes,processes,identity
Habit becomes part of identity
Habits are compound interest if self improvement.

Take an example of sport, When playing sport goal is to have the best score but it is rediculous to spend all game looking at the scoreboard beacause it could not help in any way

To build an habit we want
Cue -> craving -> response -> reward

Time magnifies the margin between success and failure

## Question 7

### Write about the book's perspective on how to make a good habit easier?

1. The 1% better every day
   Its a power of compounding both in positive and negative way
   If we follow 1% better every day after a year we end up by 37 times better by the time we are done.
   If 1% worse every day after year it declines to zero.

Focus on process than outcome make habit easier

## Question 8

### Write about the book's perspective on making a bad habit more difficult?

When tiny good habits are doing daily causes bad habits make enemy
Take more steps to do that activity

# 5. Reflection:

## Question 9:

### Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

I pick an habit of reading theory question on JavaScript technology

To make it easier I prepare some questions before itself
End up writing atleast one line

## Question 10:

### Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

I like to eliminate open youtube in free time every day

Make folders to that app
Logout from app causes not recommend any videos related to previous watch.
