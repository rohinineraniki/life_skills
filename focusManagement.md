# Focus Management

## Question 1

### What is Deep Work?

Focusing without distractions on task is deep work. Focus is tier-1 skill. Example of deep work is mathematicians, theoretical physicists, coding, they have lot of tools need to combine them in creative way.

Publishing book also required focus for a person.

Undistracted concentration is importent everywhere.

## Question 2

### Paraphrase all the ideas in the above videos and this one in detail.

About context switch
If we are working with library which is relevent to current topic it is not context switch, here we are thinking about coding and solutions

Email answering is like context switching because all emails have different context

In cal Newport point of view optimal duration for deep work is at least 1 hour, this give good focus of 40-45 minutes

Deadline in work place is like motivating line. It is like creating process giving yourself over to the process then we have strict to that.

But being pressurized is not good.Train yourself to appreciate some things.

A book called deep work written by Cal Newport

Deep work means distraction-free it imporves skills,creates new value and hard for others to replicate.

JK Rowling written a last book of Harry Potter with deep work
Bill gates work deeply when he develop microsoft

Neuroscientists found that intence period of focus in isolated fields of work causes to delvelop myelin is white tissue in brain

Deep work requires undivided attention.

3 deep work strategies are

1. Schedule distractions
2. Deep work ritual - means generate a time period repetition in every day
   it becomes rithamic. Consistently complete 4 hours of deep work per day the same time every day
3. Evening shutdown - Make a note of unfinished tasks and action plan for next day. Shutdown for a day.

## Question 3

### How can you implement the principles in your day to day life?

I will make schedule things which distract me
I will generate rithmic deep work timings
Make a shut down all work after some point of time in evening

## Question 4

### Your key takeaways from the video

Dr. Cal Newport ted talk

He dont use social media and recommend to quit.

Objections to quit social media are

1. Social media is fundamental technology
   Cal Newport answer - Social media people hires some persons to make more attractive pictures and grab attension and make profit

2. Social media is vital to someone success in 21st century economy
   Cal Newport answer - Market give value for those who are deep, concentrated, to build new skills.
   working with big data requires deep work

3. Its harmless and fun to use it
   Cal Newport answer - Social media tools are designed to be addictive. Large portion of your day breaking up our attention to check messages or any thing, it permanently reducing your capacity to attention main things.
   More using social media more likely isolated and lonely its psycological negative impacts.

Life without social media is quite positive. It is very productive.

Key takeaways:

Sometimes my phone grabing my focus, I dont give a chance for next time.

Market values for most productive person.
