# 1. Grit

## Question 1

### Paraphrase (summarize) the video in a few lines. Use your own words.

The power of passion and perseverance:
In this video Angela Lee Duckworth told about grit and growth mindset.
She shifted her job from management consultation to teaching, she observed in teaching that all the students not have same IQ, some of the students who scores more, dont have less IQ some of the students who scores less, dont have more IQ.

After several years of her teaching she came to conclusion that is in education much better understading needed from motivational, psychological perspective. In education we can take IQ as best measure.

Doing good in both education and life needs some extra qualities.

She later went for graduation to become psychologist. She take some practicals with milatary academy, competitions, teachers,salesforce peoples. After all these one thing emerged for success is grit.

Grit is passion and perseverance for very log-term goals. Grit is having stamina. It is like marathon.

#### How grit in reality?

Grit is not about talent. Building grit in kid is by Growth mindset, it is the belif that the ability of learn is not fixed,
when kids have face more challenges and they grow they are much more perseverence when they fail.

## Question 2

### What are your key takeaways from the video to take action on?

Grit plays importent role for long term goals, grit developes from growth mindset.
I develop growth mindset and more perseverance to failures.

# 2. Introduction to Growth Mindset

## Question 3

### Paraphrase (summarize) the video in a few lines in your own words.

Growth Mindset:
It is belief in your capacity to learn and grow, Carol Dweck is Professor of Psychology told that " For decades I,ve been studying why some people succeed while people,who are equally talented, do not. And over the years I have discovered that people's mindset plays crucial role in this process. "

- Fixed Mindset:
  The persons who have fixed mindset they think skills are born.They believe that you are not in control of your abilities.

- Growth Mindset:
  The persons who have growth mindset they think skills can be built. They believe that you are in control of your abilities.

I am attaching a picture below gives the difference between fixed and growth mindsets.

![Alt text](https://i.imgur.com/UEqhsLQ.png)

## Question 4

### What are your key takeaways from the video to take action on?

To develop growth mindset I have to change some beliefs which are blocking my growth process.
Someone performing well means they are not born with it, they learned so I can also learn new skills at any point of time. For this focus comes into picture if I work with more focus I can do better.
In the Process of learning skills it takes effectiveness, challenges, mistakes and we get feedback from others also.
Each step is important to get new skill reacting positively in each step gives me growth mindset.

# 3. Understanding Internal Locus of Control

## Question 5

### What is the Internal Locus of Control? What is the key point in the video?

Internal locus of control is the degree to which you belive you have control over a life.It helps to keep staying motivated.

Two different people of have external locus of control and internal focus of control, the people who have external locus of control belive that they performed or succeed in any tasks because of they are smart,born with skills and talent. People with this kind of mind giveup easily.

The people who have internal locus of control control belive that they performed or succeed in any tasks because of hardwork they did, and efforts.

Key Point:
Anything happens in life is because of things what we did and our efforts not an external factors totally.

Example: Salesperson,how a person increase sleep time.

# 3. How to build a Growth Mindset

## Question 6

### Paraphrase (summarize) the video in a few lines in your own words.

Growth Mindset in his prospective is

1. Believe the things what you can do and figure it out where do you do better it gives life time growth.

2. Question your assumptions,
   Assumptions like you are not capable to do that, and being introvert these needs to be overcome by questioning our capabilities.

3. We have to bulid our own life curriculum to develop longterm growth mindset. Build small blocks to make our passion possible in real life. Become strong enough to achieve goals. When we started to become grow we get open mindset allows others ideas and prospectives.

When started to grow many difficulties and struggles need to face but dont quit.

## Question 7

### What are your key takeaways from the video to take action on?

I can improve my growth mindset firstly I remove all external beliefs and will think what are the barriers for my growth.

I will practice more on the things when I am lagging to improve my capability. I question my self when I am unable to do or struck somewhere.

Open mindset is also one of the keys to grow. So accepting new ideas from other people and getting feedback leads to grow.

# 4. Mindset - A MountBlue Warrior Reference Manual

## Question 8

### What are one or more points that you want to take action on from the manual? (Maximum 3)

I will take ownership of the projects assigned to me. Its execution, delivery and functionality is my sole responsibility.

I will understand the users very well. I will serve them and the society by writing rock solid excellent software.

I will follow the steps required to solve problems:

- Relax
- Focus - What is the problem? What do I need to know to solve it?
- Understand - Documentation, Google, Stack Overflow, Github Issues, Internet
- Code
- Repeat
