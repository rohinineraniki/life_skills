# Question 1

## What is the Feynman Technique? Paraphrase the video in your own words.

There are 4 steps involved mainly in Feyman Technique.

1. First take a piece of paper and write topic name first at top.
2. Explaining that concept in simple way.
3. Take a note of points where we are not sure about that concepts,then go back to resourses to learn more.
4. Practice on complex terms and try to explain in easy way.

# Question 2

## What are the different ways to implement this technique in your learning process?

- When I start to learn new things I make a note of importent points and resourses also.
  Example: When I started learn new things I searched more in google and I get good points but cant take a note of resourses so I cant go back when I get doughts.
- I will explain it myself infront of mirror and watch it again. So I can get what are mistakes I made and rectify it.
- Simplify things by questioning like why this is happen and What are the different ways to do it.

# Question 3

## Paraphrase the video in detail in your own words.

One person is not have proficient knowledge become an professor.
In her life travel she met with people who have good problem solving techniques.
She thought to change her mind "Why can't I do that", She already have a knowledge in russian language and want to extend to maths , science.
She decided to change her mind to learn new things.

Keys to learn new things:

- Focus mode
- diffuse mode

In **Focus mode** we strict with some history patterns in our mind.If you strucked anywhere
it need to switch diffuse mode.

In **Diffuse mode** relax and think freely to work on new ideas.

When we learning new things we have to switch between these two modes.

Some people have procastination,There is 2 ways to handle this.

1. Keeping working through it,after some time it will disappear.
2. Taking our attention away, on temporary happines but if we do this more times may be it becomes an addiction. To overcome it we have pomodoro techinque.

So we can develop both ability to focus and ability to relax.

### Idea of Memory:

We cant hold our ideas for more ideas.In positive way it is more creative note why because we get more new ideas oftenly.

Slow thinking:

Take an example of hiker compare to racer.Hiker have many deeper experiences.

Effective learning:

- By doing exercise.
- Tests
- Recall

# Question 4

## What are some of the steps that you can take to improve your learning process?

- I follow the method of focus and diffuse mode.
- Do learn and leave so keep doing exercise will give more insights.
  -I will follow pomodoro technique to improve focus and relax.

# Question 5

## Your key takeaways from the video? Paraphrase your understanding.

How long it take to learn new skill?
10000 hours - It is according to some authors. But this 10000 hours required to become an expert level and world class.

Reasonbly good at learning new things take 20 hour of time with fully focus, It need not at 1 or 2 days,It is like daily 1 hour with total focus. And practicing things also importent.
Ways to practice it efficiently and intelligently.

- Deconstruct the skill
- Learn enough to self-correct
- Remove practice barriers
- Practice atleast 20 hours

The barrier to learn new things is at initially we scared to learn new things, so this is a emotional barrier. It takes some effort to overcome.

# Question 6

## What are some of the steps that you can while approaching a new topic?

1. I prefer to listen classes from any resource initially,I dont jump directly to read why because some misunderstadings is happens between concept and me.
2. Then I searched for standard resources beacause reading every corner gives boundaries and I cant miss them. Because non-standard things gives only overview of the concept.
3. Then I will start practice It gives more insights on that topic.
